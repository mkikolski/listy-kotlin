package pl.mkikolski.listykotlin.lista4aplikacje

open class Car(
    brand: String,
    model: String,
    productionYear: Int,
    engineType: EngineType,
    cubicCapacity: Double,
    horsePower: Int,
    ownerFirstName: String,
    ownerLastName: String,
    incidents: Int
) : Vehicle(), VehicleInterface {
    var ownerFirstName : String = ""
    var ownerLastName : String = ""
    var incidents : Int = 0


    init {
        this.brand = brand
        this.model = model
        this.productionYear = productionYear
        this.engineType = engineType
        this.cubicCapacity = cubicCapacity
        this.horsePower = horsePower
        this.ownerFirstName = ownerFirstName
        this.ownerLastName = ownerLastName
        this.incidents = incidents
    }

    override fun showInfo() {
        println("Car(ownerFirstName='$ownerFirstName', ownerLastName='$ownerLastName', incidents=$incidents), brand=$brand, model=$model")
    }

    override fun changeOwner(newOwnerFirstName: String, newOwnerLastName: String) {
        this.ownerFirstName = newOwnerFirstName
        this.ownerLastName = newOwnerLastName
    }

    override fun increaseIncidents() {
        this.incidents++
    }

    open fun getInsuranceCost() : Double {
        val cost = ((incidents + 1) * 1.5) * 5 * cubicCapacity + horsePower - this.getAge() * 10 + 600
        return cost
    }
}