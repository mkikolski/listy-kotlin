package pl.mkikolski.listykotlin.lista4aplikacje

class Truck(brand: String, model: String, productionYear: Int, engineType: EngineType,
            cubicCapacity: Double, horsePower: Int, ownerFirstName: String, ownerLastName: String,
            incidents: Int, maxLoad: Int
) : Car(
    brand, model, productionYear, engineType, cubicCapacity, horsePower, ownerFirstName,
    ownerLastName, incidents
) {
    var maxLoad: Int = 0
    var weightClass: WeightClass

    init {
        this.maxLoad = maxLoad
        if (maxLoad < 900) {
            weightClass = WeightClass.LIGHT
        } else if (maxLoad < 1700) {
            weightClass = WeightClass.MEDIUM
        } else if (maxLoad < 3000) {
            weightClass = WeightClass.HEAVY
        } else {
            weightClass = WeightClass.EXTRA_HEAVY
        }
    }

    override fun showInfo() {
        println("Truck(ownerFirstName='$ownerFirstName', ownerLastName='$ownerLastName', incidents=$incidents), brand=$brand, model=$model, maxLoad=$maxLoad")
    }

    override fun getInsuranceCost(): Double {
        return super.getInsuranceCost() * 2 + maxLoad / 2
    }

    fun additionalCost() : Double {
        // return if (weightClass == WeightClass.LIGHT) 2.0 * maxLoad else if (weightClass == WeightClass.MEDIUM) 2.5 * maxLoad else if (weightClass == WeightClass.HEAVY) 3.0 * maxLoad else 4.0 * maxLoad
        var cost = 0.0
        if (weightClass == WeightClass.LIGHT) {
            cost = 2.0 * maxLoad / 400
        } else if (weightClass == WeightClass.MEDIUM) {
            cost = 2.5 * maxLoad / 400
        } else if (weightClass == WeightClass.HEAVY) {
            cost = 3.0 * maxLoad / 400
        } else {
            cost = 4.0 * maxLoad / 400
        }
        return cost
    }
}