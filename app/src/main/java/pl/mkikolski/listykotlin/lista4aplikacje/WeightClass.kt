package pl.mkikolski.listykotlin.lista4aplikacje

enum class WeightClass {
    LIGHT,
    MEDIUM,
    HEAVY,
    EXTRA_HEAVY
}