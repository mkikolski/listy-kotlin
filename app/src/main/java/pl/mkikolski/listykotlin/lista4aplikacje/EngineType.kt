package pl.mkikolski.listykotlin.lista4aplikacje

enum class EngineType {
    ELECTRIC,
    COMBUSTION,
    DIESEL,
    HYBRID
}