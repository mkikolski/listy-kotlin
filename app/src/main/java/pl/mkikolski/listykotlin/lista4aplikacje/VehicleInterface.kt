package pl.mkikolski.listykotlin.lista4aplikacje

interface VehicleInterface {
    fun changeOwner(newOwnerFirstName: String, newOwnerLastName: String)
    fun increaseIncidents()
}