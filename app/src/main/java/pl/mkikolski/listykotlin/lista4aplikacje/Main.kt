package pl.mkikolski.listykotlin.lista4aplikacje

fun main() {
    var c1 = Car(
        "Ford",
        "Mondeo",
        2013,
        EngineType.DIESEL,
        2.0,
        160,
        "Mikolaj",
        "K",
        0
    )

    var t1 = Truck(
        "Tatra",
        "600x",
        1972,
        EngineType.COMBUSTION,
        12.0,
        380,
        "Mikolaj",
        "K",
        12,
        4200
    )

    c1.showInfo()
    println(c1.getAge())
    println(c1.getInsuranceCost())

    t1.showInfo()
    println(t1.getAge())
    println(t1.getInsuranceCost())
    println(t1.additionalCost())
}