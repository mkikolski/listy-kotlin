package pl.mkikolski.listykotlin.lista4aplikacje

import java.util.Calendar

abstract class Vehicle() {
    var brand: String = ""
    var model: String = ""
    var productionYear: Int = 2000
    var engineType: EngineType = EngineType.COMBUSTION
    var cubicCapacity: Double = 1.6
    var horsePower: Int = 120

    open fun showInfo() {
        println("Vehicle(brand='$brand', model='$model', productionYear=$productionYear, engineType=$engineType, cubicCapacity=$cubicCapacity, horsePower=$horsePower)")
    }

    fun getAge() : Int {
        return Calendar.getInstance().get(Calendar.YEAR) - productionYear
    }
}