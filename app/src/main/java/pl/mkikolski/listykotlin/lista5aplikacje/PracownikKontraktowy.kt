package pl.mkikolski.listykotlin.lista5aplikacje

class PracownikKontraktowy (
    imie: String,
    nazwisko: String,
    wiek: Int,
    szczegoly: SzczegolyPracownikaKontraktowego
) : Pracownik<SzczegolyPracownikaKontraktowego> (
    TypPracownika.KONTRAKTOWY,
    imie,
    nazwisko,
    wiek,
    szczegoly
) {
    override fun miesieczneWynagrodzenie(): Int {
        return szczegoly.stawkaZaKontrakt / szczegoly.dlugoscKontaktu
    }
}