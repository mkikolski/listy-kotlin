package pl.mkikolski.listykotlin.lista5aplikacje

class SystemZarzadzania<T> : ZarzadzenieBazaPracownikow<T> {
    val bazaPracownikow: MutableList<Pracownik<T>> = mutableListOf()
    override fun dodajPracownika(pracownik: Pracownik<T>) {
        bazaPracownikow.add(pracownik)
    }

    override fun usunPracownika(id: Int) {
        if (id > bazaPracownikow.size - 1) throw IndexOutOfBoundsException("Pracownik o podanym indeksie nie istnieje")
        bazaPracownikow.removeAt(id)
    }

    override fun usunPracownika(pracownik: Pracownik<T>) {
        bazaPracownikow.remove(pracownik)
    }

    override fun znajdzPracownikow(imie: String, nazwisko: String): List<Pracownik<T>> {
        val listaPracownikow : MutableList<Pracownik<T>> = mutableListOf()
        for (p in bazaPracownikow) {
            if (p.imie == imie && p.nazwisko == nazwisko) {
                listaPracownikow.add(p)
            }
        }
        return listaPracownikow
    }

    override fun wyswietlPracownikow() {
        for (p in bazaPracownikow) {
            println(p.toString())
        }
    }
}