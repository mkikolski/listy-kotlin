package pl.mkikolski.listykotlin.lista5aplikacje

abstract class Pracownik<T> (
    val typPracownika: TypPracownika,
    val imie: String,
    val nazwisko: String,
    val wiek: Int,
    val szczegoly: T
) {
    override fun toString(): String {
        return "$imie $nazwisko, $wiek lat"
    }

    abstract fun miesieczneWynagrodzenie() : Int

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Pracownik<*>

        if (typPracownika != other.typPracownika) return false
        if (imie != other.imie) return false
        if (nazwisko != other.nazwisko) return false
        if (wiek != other.wiek) return false
        if (szczegoly != other.szczegoly) return false

        return true
    }

    override fun hashCode(): Int {
        var result = typPracownika.hashCode()
        result = 31 * result + imie.hashCode()
        result = 31 * result + nazwisko.hashCode()
        result = 31 * result + wiek
        result = 31 * result + (szczegoly?.hashCode() ?: 0)
        return result
    }


}