package pl.mkikolski.listykotlin.lista5aplikacje

interface ZarzadzenieBazaPracownikow<T> {
    fun dodajPracownika(pracownik: Pracownik<T>)
    fun usunPracownika(id: Int)
    fun usunPracownika(pracownik: Pracownik<T>)
    fun znajdzPracownikow(imie: String, nazwisko: String) : List<Pracownik<T>>
    fun wyswietlPracownikow()
}