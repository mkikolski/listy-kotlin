package pl.mkikolski.listykotlin.lista5aplikacje

class PracownikStaly(
    imie: String,
    nazwisko: String,
    wiek: Int,
    szczegoly: SzczegolyPracownikaStalego
) : Pracownik<SzczegolyPracownikaStalego> (
    TypPracownika.STALY,
    imie,
    nazwisko,
    wiek,
    szczegoly
) {
    override fun miesieczneWynagrodzenie(): Int {
        return szczegoly.pensja
    }
}