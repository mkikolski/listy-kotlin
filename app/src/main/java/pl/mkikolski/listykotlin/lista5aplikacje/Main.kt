package pl.mkikolski.listykotlin.lista5aplikacje

fun main() {
    val systemZarzadzaniaStalych = SystemZarzadzania<SzczegolyPracownikaStalego>()
    val systemZarzadzaniaKontraktowych = SystemZarzadzania<SzczegolyPracownikaKontraktowego>()

    systemZarzadzaniaStalych.dodajPracownika(PracownikStaly(
        "Marek", "Kubiak", 49, SzczegolyPracownikaStalego("kierownik", 17800)
    ))
    systemZarzadzaniaStalych.dodajPracownika(PracownikStaly(
        "Adam", "Kowalski", 21, SzczegolyPracownikaStalego("mechanik", 5200)
    ))
    systemZarzadzaniaStalych.dodajPracownika(PracownikStaly(
        "Anna", "Nowak", 31, SzczegolyPracownikaStalego("księgowa", 9200)
    ))
    systemZarzadzaniaStalych.dodajPracownika(PracownikStaly(
        "Anna", "Nowak", 64, SzczegolyPracownikaStalego("sprzątaczka", 4600)
    ))
    systemZarzadzaniaStalych.dodajPracownika(PracownikStaly(
        "Mirosław", "Król", 58, SzczegolyPracownikaStalego("mechanik", 5900)
    ))
    systemZarzadzaniaStalych.dodajPracownika(PracownikStaly(
        "Marek", "Kubiak", 29, SzczegolyPracownikaStalego("mechanik", 5200)
    ))

    systemZarzadzaniaStalych.wyswietlPracownikow()
    println(systemZarzadzaniaStalych.znajdzPracownikow("Anna", "Nowak"))
    systemZarzadzaniaStalych.usunPracownika(1)
    systemZarzadzaniaStalych.usunPracownika(PracownikStaly(
        "Anna", "Nowak", 64, SzczegolyPracownikaStalego("sprzątaczka", 4600)
    ))
    systemZarzadzaniaStalych.wyswietlPracownikow()
}