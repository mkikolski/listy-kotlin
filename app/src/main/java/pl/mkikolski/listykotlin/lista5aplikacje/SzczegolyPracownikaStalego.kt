package pl.mkikolski.listykotlin.lista5aplikacje

data class SzczegolyPracownikaStalego(
    val stanowisko: String,
    val pensja: Int
)
