package pl.mkikolski.listykotlin.lista5aplikacje

data class SzczegolyPracownikaKontraktowego(
    val stanowisko: String,
    val dlugoscKontaktu: Int,
    val stawkaZaKontrakt: Int
)
