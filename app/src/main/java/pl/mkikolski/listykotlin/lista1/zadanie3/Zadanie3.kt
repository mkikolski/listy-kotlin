package pl.mkikolski.listykotlin.lista1.zadanie3

import kotlin.math.pow

/**
 * Funkcja zwracająca wszystkie podzbiory (zbiór potęgowy) podanego zbioru
 * W opracowaniu funkcji wykorzystano informacje zawarte na stronie https://www.geeksforgeeks.org/power-set/
 *
 * @author  Mikołaj Kikolski
 * @param   x   zbiór wejściowy
 * @return  zbiór potęgowy zbioru x
 */
fun <T> podzbiory(x: Set<T>) : Set<Set<T>> {
    val len = 2.0.pow(x.size).toInt() - 1
    val powerset = mutableSetOf<MutableSet<T>>()

    for (mask in 0..len) {
        val subset = mutableSetOf<T>()
        for (i in x.indices) {
            if ((mask and (1 shl i)) > 0) {
                subset.add(x.elementAt(i))
            }
        }
        powerset.add(subset)
    }
    return powerset
}

fun main() {
    println(podzbiory(setOf<Int>()))
}