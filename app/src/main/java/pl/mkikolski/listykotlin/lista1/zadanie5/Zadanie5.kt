package pl.mkikolski.listykotlin.lista1.zadanie5

import java.util.Collections.max

/**
 * Funkcja generuje ciąg Collatza dla podanej liczby startowej.
 *
 * W rozwiązaniu wykorzystano odpowiedź użytkownika Hasan A Yousef na pytanie umieszczone pod adresen
 * https://stackoverflow.com/questions/35522590/take-last-n-element-in-kotlin
 *
 * @author  Mikołaj Kikolski
 * @param   c0   liczba startowa ciągu Collatza
 * @return  lista liczb ciągu Collatza, zaczynając od liczby startowej, a kończąc na trójce (4, 2, 1)
 * @throws  IllegalArgumentException    w przypadku podania liczby startowej <= 0
 */
fun collatz(c0: Int) : List<Int> {
    if (c0 <= 0) throw IllegalArgumentException("Liczba startowa musi być większa od 0")

    val terms = mutableListOf(c0)

    while (terms.takeLast(3) != listOf(4, 2, 1)) {
        val lastTerm = terms.takeLast(1)[0]
        if (lastTerm % 2 == 0) {
            terms.add(lastTerm / 2)
        } else {
            terms.add(3 * lastTerm + 1)
        }
    }

    return terms
}
fun main() {
    var maxLen = 0
    var maxTerm = 0
    var maxTermC0 = 0
    var maxLenC0 = 0
    var maxLenList = listOf<Int>()
    var maxTermList = listOf<Int>()

    for (c0 in 1..20000) {
        val result = collatz(c0)
        if (result.size > maxLen) {
            maxLen = result.size
            maxLenC0 = c0
            maxLenList = result
        }

        if (max(result) > maxTerm) {
            maxTerm = max(result)
            maxTermC0 = c0
            maxTermList = result
        }
    }

    println("Największą długosć ciągu (${maxLen}) osiągnięto dla c0 = ${maxLenC0}, ciąg miał postać:")
    println(maxLenList)
    println("Największy wyraz w ciągu (${maxTerm}) osiągnięto dla c0 = ${maxTermC0}, ciąg miał postać:")
    println(maxTermList)
}