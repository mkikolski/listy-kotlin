package pl.mkikolski.listykotlin.lista1.zadanie4

/**
 * Funkcja generuje n pierwszych liczb ciągu Fibonacciego w sposób iteracyjny.
 *
 * @author  Mikołaj Kikolski
 * @param   n   ilość liczb ciągu Fibonacciego do wygenerowania
 * @return  lista n pierwszych liczb ciągu Fibonacciego
 * @throws  IllegalArgumentException    w przypadku podania n <= 0
 */
fun fibbIter(n : Int) : List<Int> {
    if (n <= 0) throw IllegalArgumentException("Ilość wyrazów ciągu do podania musi być większa od 0")
    if (n == 1) return listOf(1)

    val fibbNumbers = mutableListOf(1, 1)

    for (i in 2..<n) {
        fibbNumbers.add(fibbNumbers[i - 2] + fibbNumbers[i - 1])
    }

    return fibbNumbers
}

/**
 * Funkcja generuje n-tą liczbę ciągu Fibonacciego w sposób rekurencyjny.
 *
 * @author  Mikołaj Kikolski
 * @param   n   numer liczby ciągu Fibonacciego do wygenerowania
 * @return  n-ta liczba ciągu Fibonacciego
 * @throws  IllegalArgumentException    w przypadku podania n < 0
 */
fun fibbRec(n : Int) : Int {
    if (n < 0) throw IllegalArgumentException("Ilość wyrazów do wygenerowania nie może być ujemna")
    if (n <= 1) {
        return n
    } else {
        return fibbRec(n - 1) + fibbRec(n - 2)
    }
}

fun main() {
    for (i in 1..12) {
        print(fibbRec(i).toString() + " ")
    }
}