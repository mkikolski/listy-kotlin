package pl.mkikolski.listykotlin.lista1.zadanie1

import java.lang.IllegalArgumentException
import kotlin.math.max
import kotlin.math.sqrt

/**
 * Funkcja oblicza wartość pola trójkąta o zadanych bokach korzystając ze wzoru Herona
 *
 * @author  Mikołaj Kikolski
 * @param   a   pierwszy z boków trójkąta
 * @param   b   drugi z boków trójkąta
 * @param   c   trzeci z boków trójkąta
 * @return  pole trójkąta
 * @throws  IllegalArgumentException    w przypadku podania długości boku <= 0 oraz w przypadku podania
 *                                      boków, z których nie da się utworzyć trójkąta
 */
fun heron(a: Double, b: Double, c: Double) : Double {
    if (a <= 0 || b <= 0 || c <= 0) throw IllegalArgumentException("Podano niedozwoloną długość boku!")
    if (a + b + c - max(a, max(b, c)) <= max(a, max(b, c))) throw IllegalArgumentException("Taki trójkąt nie może istnieć!")

    val p = (a + b + c) / 2
    return sqrt(p * (p - a) * (p - b) * (p - c))
}
fun main() {
    println(heron(1.0, 1.0, 11.0))
}