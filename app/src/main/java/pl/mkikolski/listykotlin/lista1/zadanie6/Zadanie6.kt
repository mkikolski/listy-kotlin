package pl.mkikolski.listykotlin.lista1.zadanie6

/**
 * Funkcja generuje komplementarny ciąg DNA dla podanego ciągu kodującego.
 *
 * @author  Mikołaj Kikolski
 * @param   kodujaca   ciąg DNA do skomplementowania
 * @return  komplementarny ciąg DNA
 * @throws  IllegalArgumentException    w przypadku znalezienia nieprawidłowego symbolu w ciągu kodującym
 */
fun komplement(kodujaca: String) : String {
    var mat = ""
    for (zas in kodujaca.uppercase()) {
        when (zas) {
            'A' -> mat += "T"
            'T' -> mat += "A"
            'C' -> mat += "G"
            'G' -> mat += "C"
            else -> throw IllegalArgumentException("Znaleziono nieprawidłowy symbol")
        }
    }
    return mat.reversed()
}

/**
 * Funkcja generuje ciąg RNA na podstawie podanego ciągu DNA (matrycowego).
 *
 * @author  Mikołaj Kikolski
 * @param   mat   ciąg DNA matrycowy do transkrypcji
 * @return  ciąg RNA powstały w wyniku transkrypcji ciągu DNA
 * @throws  IllegalArgumentException    w przypadku znalezienia nieprawidłowego symbolu w ciągu DNA
 */
fun transkrybuj(mat: String) : String {
    var rna = ""
    for (zas in mat.uppercase()) {
        when (zas) {
            'A' -> rna += "U"
            'T' -> rna += "A"
            'C' -> rna += "G"
            'G' -> rna += "C"
            else -> throw IllegalArgumentException("Znaleziono nieprawidłowy symbol")
        }
    }
    return rna.reversed()
}

fun transluj(mrna: String) : String {
    val slownikAmino = mapOf(
        "UUU" to "Phe", "UUC" to "Phe",
        "UUA" to "Leu", "UUG" to "Leu", "CUU" to "Leu", "CUC" to "Leu", "CUA" to "Leu", "CUG" to "Leu",
        "AUU" to "Ile", "AUC" to "Ile", "AUA" to "Ile",
        "AUG" to "Met",
        "GUU" to "Val", "GUC" to "Val", "GUA" to "Val", "GUG" to "Val",
        "UCU" to "Ser", "UCC" to "Ser", "UCA" to "Ser", "UCG" to "Ser", "AGU" to "Ser", "AGC" to "Ser",
        "CCU" to "Pro", "CCC" to "Pro", "CCA" to "Pro", "CCG" to "Pro",
        "ACU" to "Thr", "ACC" to "Thr", "ACA" to "Thr", "ACG" to "Thr",
        "GCU" to "Ala", "GCC" to "Ala", "GCA" to "Ala", "GCG" to "Ala",
        "UAU" to "Tyr", "UAC" to "Tyr",
        "UAA" to "Stop", "UAG" to "Stop", "UGA" to "Stop",
        "CAU" to "His", "CAC" to "His",
        "CAA" to "Gln", "CAG" to "Gln",
        "AAU" to "Asn", "AAC" to "Asn",
        "AAA" to "Lys", "AAG" to "Lys",
        "GAU" to "Asp", "GAC" to "Asp",
        "GAA" to "Glu", "GAG" to "Glu",
        "UGU" to "Cys", "UGC" to "Cys",
        "UGG" to "Trp",
        "CGU" to "Arg", "CGC" to "Arg", "CGA" to "Arg", "CGG" to "Arg", "AGA" to "Arg", "AGG" to "Arg",
        "GGU" to "Gly", "GGC" to "Gly", "GGA" to "Gly", "GGG" to "Gly"
    )
    var rna = mrna.uppercase()
    if (!rna.matches(regex = Regex("^[AUCG]+\$"))) throw IllegalArgumentException("Sekwencja RNA powinna zawierać jedynie A, U, G, C")
    var prot = ""
    while (rna.length >= 3) {
        val code = rna.subSequence(0, 3)
        val amino = slownikAmino[code]
        if (amino == "Stop") return prot
        prot += amino
        rna = rna.drop(3)
    }
    return prot
}

fun testTranslujKodonStop() {
    val rna = "AAACAUCAUUAGUCACGA"
    val wynik = transluj(rna)
    assert(wynik == "LysHisHis", lazyMessage = { "Test translujKodonStop niezaliczony, otrzymano ${wynik} zamiast \"LysHisHis\"" })
    println("Test translujKodonStop został zaliczony")
}

fun testTranslujWyjatek() {
    val rna = "AAAR"

    try {
        val wynik = transluj(rna)
    } catch (e: IllegalArgumentException) {
        println("testTranslujWyjątek został zaliczony. Poprawnie przechwycono: ${e.message}")
        return
    }
    println("testTranslujWyjątek niezaliczony")
}

fun main() {
    val kodujaca = "ATCGAGTC"
    val mat = komplement(kodujaca)
    val rna = transkrybuj(mat)
    println(kodujaca)
    println(mat)
    println(rna)
    testTranslujKodonStop()
    testTranslujWyjatek()
}