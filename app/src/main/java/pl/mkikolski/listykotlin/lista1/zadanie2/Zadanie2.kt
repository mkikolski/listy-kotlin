package pl.mkikolski.listykotlin.lista1.zadanie2

/**
 * Funkcja zwraca zbiór elementów wspólnych dla dwóch multizbiorów.
 *
 * @author  Mikołaj Kikolski
 * @param   x   pierwszy multizbiór
 * @param   y   drugi multizbiór
 * @return  multizbiór elementów, które występują zarówno w x i y
 */
fun <T> wspolne(x: List<T>, y: List<T>) : List<T> {
    var czescWspolna = mutableListOf<T>()

    for (el in x) {
        if (y.contains(el)) {
            czescWspolna.add(el)
        }
    }

    return czescWspolna
}

fun main() {
    println(wspolne(listOf(1, 2, 3, 4, 4), listOf(2, "a", 4, 4, 4)))
}