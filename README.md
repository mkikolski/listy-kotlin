# Rozwiązania list

Będę tu wrzucał kod wypracowany w ramach moich streamów/nagrań z tłumaczenia Kotlina. Repozytorium kodu w zamyśle służy tu jako dodatkowa ilustracja do moich prób tłumaczenia, a nie źródło do skopiowania, ale to Wy decydujecie co z nim zrobicie. W razie problemów/potrzeby dodatkowych wyjaśnień/czegokolwiek:
- piszcie na [Messengerze](https://m.me/pomidoraj)
- piszcie komentarze pod konkretnymi filmami z podanym timestampem

## Jak korzystać z tego repozytorium?
Jeśli korzystacie z tego kodu wprost, a nie jako z materiału pomocniczego, szczególnie w kontekście laboratorium JPDZBM gdzie rozwiązujeie problemy bardziej z dziedziny pisania algorytmów to zalecam:
- otwarte przyznanie się skąd on jest
- próbę zrozumienia kodu z nagrań, na których staram się go tłumaczyć
- w przypadku niezrozumienia jakiegoś fragmentu kodu, zadanie pytania tak jak opisywalem wyżej

Głupie pytania istnieją, ale odpowiadam na każde bez oceniania osób pytających, bo sam kiedyś startowałem z programowaniem i wiem jak skomplikowane to może być.

## Lista filmów
- [Lista 1, zadania 1, 2, 4 i 5](https://youtu.be/OLdVhBFNs_U)
- [Lista 1, zadania 3 i 6](https://youtu.be/JPNlOmIgF5k)
- [Lista 4, aplikacje](https://youtu.be/p2xQExLavUw)
- [Lista 5, aplikacje](https://youtu.be/9U_rGWSKBxI)

## FAQ

Kiedy będzie filmik z listą X/zadaniem X?
> Z reguły piszę na grupie kierunkowej/mówię na poprzednim filmiku, ale jak trzeba coś na szybko to największą szansę na załatwienie tego macie na [Messengerze](https://m.me/pomidoraj)

Jak uzyskać Copilota?
> [Tu na samym początku jest jak](https://youtu.be/OLdVhBFNs_U)

Potrzebuję lepszego tłumaczenia tematu X/pomocy z wykonaniem projektu...
> Napisz na [Messengerze](https://m.me/pomidoraj), kiedy tylko mam czas to chętnie pomogę